---
type: page
layout: about
title: About
---

I use this page to write about personal projects I work on in my free time. Check out the [posts](/posts/) page for complete list of articles or the [tags](/tags/) page for a list of topics. 

You can contact me via {{< antispammail "hi@davekoch.de" >}}.