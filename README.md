create a new post:
```bash
hugo new content/posts/my_new_post.md
```

hot reloading with drafts included:
```bash
hugo server -D
```

build: `hugo`

Remember to set `draft: false` if you actually want to publish.